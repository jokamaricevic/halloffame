# Hall Of Fame

Hall Of Fame is an android java application made for the purpose of performing tasks for college.

## Description

It's simply made in a single activity and displays the 3 most significant persons in my life. In addition to basic information, it also provides a description that can be modified using a simple UI. Also, functionality has been added to remove images by clicking on them.
