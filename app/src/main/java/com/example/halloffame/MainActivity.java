package com.example.halloffame;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private ImageView image1;
    private ImageView image2;
    private ImageView image3;
    private TextView description1;
    private TextView description2;
    private TextView description3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initViews();

        //Hide images on click
        hideImg(image1);
        hideImg(image2);
        hideImg(image3);

        //Change description
        buttonDescription();

        //Inspiration
        buttonInspiration();



    }

    void initViews(){
        this.image1 = findViewById(R.id.image1);
        this.image2 = findViewById(R.id.image2);
        this.image3 = findViewById(R.id.image3);
        this.description1 = findViewById(R.id.description1);
        this.description2 = findViewById(R.id.description2);
        this.description3 = findViewById(R.id.description3);
    }

    void showToast(){
        String[] strings = new String[] {getString(R.string.albert1),
                                         getString(R.string.albert2),
                                         getString(R.string.albert3),
                                         getString(R.string.tesla1),
                                         getString(R.string.tesla2),
                                         getString(R.string.tesla3),
                                         getString(R.string.isaac1),
                                         getString(R.string.isaac2),
                                         getString(R.string.isaac3),};

        String inspiration = strings[new Random().nextInt(strings.length)];
        Toast.makeText(getApplicationContext(), inspiration, Toast.LENGTH_LONG).show();
    }

    void buttonInspiration(){
        Button button = findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showToast();
            }
        });
    }


    void buttonDescription(){
        Button button = findViewById(R.id.button2);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeDescription();
            }
        });
    }

    void changeDescription(){
        TextView description = getDescription();
        EditText newDescription = findViewById(R.id.editText);
        description.setText(newDescription.getText());
        newDescription.setText("");
    }

    TextView getDescription(){
        String person = checkRadioButtons();

        TextView description = new TextView(this);
        if(person == "first") description = description1;
        else if(person == "second") description = description2;
        else if(person == "third") description = description3;
        return description;
    }

    String checkRadioButtons(){
        RadioButton person1 = findViewById(R.id.person1);
        RadioButton person2 = findViewById(R.id.person2);
        RadioButton person3 = findViewById(R.id.person3);
        String person = "";
        if(person1.isChecked()) person = "first";
        else if(person2.isChecked()) person = "second";
        else if(person3.isChecked()) person = "third";
        return person;

    }

    void hideImg(ImageView imgView){
        final ImageView imageView = imgView;
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageView.setVisibility(View.INVISIBLE);
            }
        });
    }



}
